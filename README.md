# SAСHY

## Overview

*Sachy* is an XBoard chess engine written in C by Martin Macok.

This is a version retouched by Dann Corbit then by Roland Chastain.

## History

### v0.2.05

* Handle correctly value zero for MOVESPERSESSION in level command

### v0.2.04

* Support underpromotion from the opponent
* Simplify pawn moves generation
* Try to improve time management

### v0.2.03

* Use [CECP v2](http://hgm.nubati.net/CECP.html)
* New Makefile which allows to choose between 32-bit and 64-bit compilation (1)
* Made a faster version of the `MakeMove` function
* Minimal thinking output for WinBoard/XBoard
* Create log files

(1) Type `make sachy32` or `make sachy64`.

### v0.2.02

* Add 'q' character to promotion moves, for compatibility with CuteChess
* Partially translated identifiers to english (thanks to Martin Sedlák for his kind help)
* Fixed compiler warnings

## Links

* [Sachy 0.2.00](http://web.archive.org/web/20021208030047/http://xtrmntr.org/ORBman/download/sachy-0.2.00.tar.bz2)
* [Sachy 0.2.01](http://computer-chess.org/lib/exe/fetch.php?media=computer_chess:wiki:download:sachy-0.2.01dc.rar)

## Logo

![alt text](https://gitlab.com/rchastain/sachy/-/raw/main/logo/sachy.jpg)
